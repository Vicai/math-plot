import matplotlib.pyplot as plt
import numpy as np

"""
条形图
"""
paper_config = {
    'mathtext.fontset': 'stix',  # 公式 Times New Roman
    'font.family': 'sans-serif',  # 默认字体族是 sans-serif 无衬线字体
    'font.sans-serif': ['Songti SC', 'Times New Roman'],  # 字体族 sans-serif 中的字体按序查找
    'font.weight': 'ultralight',
    'font.size': 18,
    'axes.unicode_minus': False,  # 避免图像是负号'-'显示为方块
}
plt.rcParams.update(paper_config)
fig = plt.figure(figsize=(15, 9), dpi=200)
"""
子图1
"""
x = np.arange(24)
weekday_24h_average = np.random.randint(low=700, high=6000, size=24)
weekday_24h_stderr = np.random.randint(low=100, high=1400, size=24)
weekend_24h_average = np.random.randint(low=900, high=9100, size=24)
weekend_24h_stderr = np.random.randint(low=300, high=2200, size=24)
error_attri = {
    "elinewidth": 1,  # 误差棒粗细
    "capsize": 5,  # 误差棒宽度
    "ecolor": "black"
}  # 误差棒的属性
bar_width = 0.4  # 柱形的宽度
tick_label = [i for i in range(1, 25)]  # 横坐标的标签
plt.bar(x, weekday_24h_average,
        bar_width,
        color='#EE8982',
        align="center",
        yerr=weekday_24h_stderr,
        error_kw=error_attri,
        label="实验组",
        alpha=1)
plt.bar(x + bar_width, weekend_24h_average,  # 若没有没有向右侧增加一个bar_width的宽度的话，第一个柱体就会被遮挡住
        bar_width,
        color="#8DB7DB",
        yerr=weekend_24h_stderr,
        error_kw=error_attri,
        label="对照组",
        alpha=1)
plt.xlabel("组别（每组/10人）")
plt.ylabel("学习成绩提升率")
plt.xticks(x + bar_width / 2, tick_label)
plt.title("24 组训练提升率")
plt.grid(axis="y", ls="--", color="black", alpha=0.5)
"""
画布
"""
plt.subplots_adjust(wspace=0.2, hspace=0.5)
plt.legend(
    fancybox=False,  # 圆角
    shadow=False,  # 阴影
    framealpha=1,  # 透明度
    title='图例',
    edgecolor='#000',  # 边框颜色
    facecolor='#fff',  # 填充颜色
    frameon=True,  # 是否显示图例框线
    loc='upper right'  # 图例位置
)
plt.show()
