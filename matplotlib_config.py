import matplotlib.pyplot as plt
import matplotlib.font_manager
import shutil

# 查看支持的字体
for font in matplotlib.font_manager.fontManager.ttflist:
    print(font.name, '-', font.fname)

config = {
    'mathtext.fontset': 'stix',  # 公式 Times New Roman
    'font.family': 'sans-serif',  # 默认字体族是 sans-serif
    'font.sans-serif': ['Songti', 'Times New Roman'],  # 字体族 sans-serif 中的字体按序查找
    'font.size': 18,
    'axes.unicode_minus': False,  # 避免图像是负号'-'显示为方块
}
plt.rcParams.update(config)
print(plt.rcParams['font.family'])
print(plt.rcParams['font.sans-serif'])
print(matplotlib.matplotlib_fname())
print(matplotlib.get_cachedir())
shutil.rmtree(matplotlib.get_cachedir())
