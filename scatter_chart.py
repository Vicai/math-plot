import matplotlib.pyplot as plt
import numpy as np

"""
散点图
"""
paper_config = {
    'mathtext.fontset': 'stix',  # 公式 Times New Roman
    'font.family': 'sans-serif',  # 默认字体族是 sans-serif 无衬线字体
    'font.sans-serif': ['SimSun', 'Times New Roman'],  # 字体族 sans-serif 中的字体按序查找
    'font.size': 18,
    'axes.unicode_minus': False,  # 避免图像是负号'-'显示为方块
}
plt.rcParams.update(paper_config)
fig = plt.figure(figsize=(15, 9), dpi=200)
"""
子图1
"""
sub_plot_1 = fig.add_subplot(121)
x = np.array([5, 7, 8, 7, 2, 17, 2, 9, 4, 11, 12, 9, 6])
y = np.array([99, 86, 87, 88, 111, 86, 103, 87, 94, 78, 77, 85, 86])
plt.scatter(x, y, color='hotpink')
x = np.array([2, 2, 8, 1, 15, 8, 12, 9, 7, 3, 11, 4, 7, 14, 12])
y = np.array([100, 105, 84, 105, 90, 99, 90, 95, 94, 100, 79, 112, 91, 80, 85])
plt.scatter(x, y, color='#88c999')
plt.title('中文：宋体 \n 英文：$\mathrm{Times \; New \; Roman}$ \n 公式： 其中，$\\alpha_i + \\beta_i = \\gamma^k$')

"""
子图2
"""
sub_plot_2 = fig.add_subplot(122)
x = np.random.rand(20)
y = np.random.rand(20)
s = np.array(range(10, 110, 5))
c = np.array(range(0, 20))
plt.scatter(x, y, s=s, c=c)

plt.colorbar()
"""
画布
"""
plt.subplots_adjust(wspace=0.5, hspace=1)
# 默认绘制在最后一个子图
plt.show()
