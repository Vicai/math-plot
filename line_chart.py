import matplotlib.pyplot as plt
import numpy as np
"""
折线图
"""
fig = plt.figure(figsize=(30, 10), dpi=80)

"""
子图1
"""
ax1 = fig.add_subplot(1, 3, 1)
ax1.plot(np.random.randn(1000).cumsum())
# 设定 X 轴刻度
ticks = ax1.set_xticks([0, 250, 500, 750, 1000])
# 设定 X 轴刻度的标签
labels = ax1.set_xticklabels(['one', 'two', 'three', 'four', 'five'], rotation=30, fontsize='small')
ax1.set_title('My first matplotlib plot')
"""
子图2
"""
ax2 = fig.add_subplot(1, 3, 2)
t = np.arange(0.0, 2.0, 0.01)
s = np.sin(2 * np.pi * t)
# 设置图示
ax2.set_title(r'$\alpha_i > \beta_i$', fontsize=20)
plt.text(0.1, 0.2, r'$\sum_{i=0}^\infty x_i$', fontsize=20)
plt.text(0.5, 0.6, r'$\mathcal{A}\mathrm{sin}(2 \omega t)$', fontsize=20)
ax2.set_xlabel('time (s)')
ax2.set_ylabel('volts (mV)')
# 去掉边框
ax2.spines['right'].set_color('none')
ax2.spines['top'].set_color('none')
# 绘制图像
ax2.plot(t, s)
"""
子图3
"""
ax3 = fig.add_subplot(1, 3, 3)
ax3.plot(np.random.randn(1000).cumsum(), 'k', label='one')
ax3.plot(np.random.randn(1000).cumsum(), 'k--', label='two')
ax3.plot(np.random.randn(1000).cumsum(), 'k.', label='three')
# 设置图例
ax3.legend(labels=['1', '2', '3'], loc='best')
"""
画布
"""
plt.subplots_adjust(wspace=0.5, hspace=1)
plt.show()
