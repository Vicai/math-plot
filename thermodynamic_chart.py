import matplotlib.pyplot as plt
import matplotlib.font_manager as fm
"""
热力图
"""
fig = plt.figure(figsize=(5, 5), dpi=300)
ch_font = fm.FontProperties(fname='/System/Library/Fonts/Supplemental/Songti.ttc')
"""
子图1
"""
ax = fig.add_subplot(1, 3, 1)
plt.title("中文：宋体", fontproperties=ch_font)

"""
画布
"""
plt.subplots_adjust(wspace=0.5, hspace=1)
# 默认绘制在最后一个子图
plt.show()

